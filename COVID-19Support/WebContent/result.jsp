<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>COVID-19 Support</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="CSS/style.css">
</head>
<body>
	<nav>
		<div>
			<a href="index.html" class="logo">COVID-19 Support</a>
		</div>
		<div class="nav-content">
			<div>
				<a href="contactUs.html">Contact Us</a>
			</div>
		</div>
	</nav>
	<br>
	<br>
	<header>
		<div class="headerContent center">
		<h1 class="center">Donate to COVID-19 Pandemic Response</h1><br><br>
			<%
				List result = (List) request.getAttribute("service");
				Iterator it = result.iterator();

				while (it.hasNext()) {
					out.println(it.next() + "      <input type='number' required>    <br><br>");
				}
			%>
			<a href='confirmation.html'><button class='button'>DONATE</button></a>
		</div>
	</header>

</body>
</html>