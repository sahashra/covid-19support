package sheridan;

public enum ServiceType {
	FoodBanks, Homeless, Hospital, Senior, MentalHealth, Children
}
