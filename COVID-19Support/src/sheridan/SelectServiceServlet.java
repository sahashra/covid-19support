package sheridan;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "selectserviceservlet", urlPatterns = "/SelectSupport")

public class SelectServiceServlet extends HttpServlet {
	private static final long serialVersionUID = -1831978487671842618L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String serviceType = req.getParameter("Type");

		SupportService Service = new SupportService();
		ServiceType s = ServiceType.valueOf(serviceType);

		List<String> Support = Service.getServiceType(s);

		req.setAttribute("service", Support);
		RequestDispatcher view = req.getRequestDispatcher("result.jsp");
		view.forward(req, resp);

	}

}
