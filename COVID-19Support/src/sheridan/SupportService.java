package sheridan;

import java.util.ArrayList;
import java.util.List;

public class SupportService {
	 public List<String> getServiceType(ServiceType type){

	        List<String> service = new ArrayList( );

	        if(type.equals(ServiceType.FoodBanks)){
	            service.add("Food Bank Canada");
	            service.add("Breakfast Club of Canada");
	            service.add("Community Food Center Canada");

	        }else if(type.equals(ServiceType.Homeless)){
	        	service.add("Covenant House Vancouver");
	        	service.add("HollyBurn Family Services Society");
	        	service.add("Network of Inner Cities");
	        	
	        }else if(type.equals(ServiceType.Hospital)){
	        	service.add("BC Women's Health Foundation");
	        	service.add("Langley Memorial Hospital Foundation");
	        	service.add("Victoria Hospitals Foundation");

	        }else if(type.equals(ServiceType.Senior)){
	        	service.add("Disaster Aid Canada");
	        	service.add("HelpAge Canada");
	        	service.add("Senior's Resource Society");

	        }else if(type.equals(ServiceType.MentalHealth)){
	        	service.add("Kids Help Phone");
	        	service.add("Anxiety Canada");
	        	service.add("Your Life Counts");

	        }else if(type.equals(ServiceType.Children)){
	        	service.add("I Can for Kids Foundation");
	        	service.add("Boys & Girls Club of the BattleFords");
	        	service.add("MindFuel - Science Alberta Foundation");

	        }else {
	            service.add("No Service Available");
	        }
	    return service;
	    }
}
